export const tareas = [{
  "id": 1,
  "title": "Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.",
  "hora": "7:43",
  "fecha": "22/03/2019",
  "tipo": 2
}, {
  "id": 2,
  "title": "Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.",
  "hora": "17:37",
  "fecha": "23/04/2019",
  "tipo": 2
}, {
  "id": 3,
  "title": "Integer ac neque. Duis bibendum.",
  "hora": "15:07",
  "fecha": "23/05/2019",
  "tipo": 2
}, {
  "id": 4,
  "title": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.",
  "hora": "3:04",
  "fecha": "06/06/2019",
  "tipo": 2
}, {
  "id": 5,
  "title": "Donec semper sapien a libero. Nam dui.",
  "hora": "10:06",
  "fecha": "23/02/2019",
  "tipo": 2
}, {
  "id": 6,
  "title": "Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.",
  "hora": "18:30",
  "fecha": "17/09/2018",
  "tipo": 3
}, {
  "id": 7,
  "title": "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.",
  "hora": "14:40",
  "fecha": "17/05/2019",
  "tipo": 1
}, {
  "id": 8,
  "title": "Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.",
  "hora": "16:15",
  "fecha": "26/09/2018",
  "tipo": 1
}, {
  "id": 9,
  "title": "Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.",
  "hora": "12:04",
  "fecha": "15/05/2019",
  "tipo": 1
}, {
  "id": 10,
  "title": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.",
  "hora": "11:34",
  "fecha": "18/01/2019",
  "tipo": 1
}];