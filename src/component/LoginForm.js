import React, { Component } from 'react'
import Logo from './IMG/Perfil1.jpg'
import './STYLES/LoginForm.css'
import {Redirect} from 'react-router-dom'

export default class LoginForm extends Component {
    state = { redirect:false, form: { fullName: '', email: '', password: ''}, errors: {name: 'Campo Obligatorio'}}
    
    handleChange = e => {
        let newForm = this.state.form;
        newForm[e.target.name]= e.target.value;
        this.setState({form: newForm});
    }

   isValidate = () => {
       if(this.state.form.fullName === '')
        {
            alert("El nombre es obligatorio");
            
            return false;
        }
        if(this.state.form.email.indexOf('@') < 1 )
        {
           alert("El email deberia contener @")
           return false;
        }
        if(this.state.form.password.length <= 8)
        {
            alert("La contraseña deberia poseer mas de 8 digitos")
            return false;
        }

        
        return true;
   }


    handleSubmit = e => {
        e.preventDefault();
        
        if(this.isValidate()){ 
            this.setState({redirect: true})
        }
    } 

    render() {
        if(this.state.redirect)
        {
            return (<Redirect to={{pathname: "/Home", state: this.state.form}}   />)
            
        }
        return (
            <div>              
                <form className="ff" onSubmit={this.handleSubmit}>
                
                <table className="tablaForm">
                
                <tr> <b>FORM </b> </tr> 
                <tr> <img src={Logo} alt= "Logo" /> </tr>
                
                <tr> FullName: </tr>
                <tr> <input type= "text" name= "fullName" value={this.state.form.fullName}  onChange = {this.handleChange}/> </tr>
                <tr> Email: </tr>
                <tr> <input type= "text" name= "email" value={this.state.form.email}  onChange = {this.handleChange}/> </tr>
                <tr> Password: </tr>
                <tr> <input type= "password" name= "password" value={this.state.form.password} onChange = {this.handleChange}/> </tr>
                <tr />
                <tr><button type="submit"  > SUBMIT </button> </tr>
               </table>
                </form> 
            </div>

       
            
        );
    }
        
}

