import React from "react";
import "./STYLES/Lista.css";
import "./STYLES/bootstrap.css";

function TareaListEnProceso(props) {
  const tareas = props.tareas;
  const eliminar = props.onDelete;
  const modificar = props.onEdit;
  const aHecho = props.aHecho;
  const aParaHacer = props.aParaHacer;

  if (tareas) {
    return (
      <div>
        <ul className="list-unstyled">
          {tareas.map(tarea => {
            return (
              <li key={tarea.id}>
                <TareasListItem
                  tarea={tarea}
                  onDelete={eliminar}
                  onEdit={modificar}
                  aHecho={aHecho}
                  aParaHacer={aParaHacer}
                />
              </li>
            );
          })}
        </ul>
      </div>
    );
  } else {
    return null;
  }
}

class TareasListItem extends React.Component {
  render() {
    if (this.props.tarea.tipo === 2) {
      return (
        <div>
          <div className="ListItemEnPrc">
            <strong>
              {this.props.tarea.id}- {this.props.tarea.title}
            </strong>{" "}
            <p>
              {this.props.tarea.hora} {this.props.tarea.fecha}{" "}
            </p>
            <button
              className="b2 btn btn-primary btn-sm"
              onClick={() => {
                this.props.aParaHacer(this.props.tarea);
              }}
            >
              Para hacer
            </button>
            <button
              className="b2 btn btn-light btn-sm "
              onClick={() => {
                this.props.onEdit(this.props.tarea);
              }}
            >
              {" "}
              Modificar{" "}
            </button>
            <button
              className="b2 btn btn-dark btn-sm "
              onClick={() => {
                this.props.onDelete(this.props.tarea);
              }}
            >
              {" "}
              Eliminar{" "}
            </button>
            <button
              className="b2 btn btn-primary btn-sm"
              onClick={() => {
                this.props.aHecho(this.props.tarea);
              }}
            >
              Hecho
            </button>
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}

export default TareaListEnProceso;
