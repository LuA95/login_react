import React, { Component } from "react";
import TareaListEnProceso from "../TareaListEnProceso";
import { tareas } from "../../data/tareas.jsx";
import TareaListPorHacer from "../TareaListPorHacer";
import TareaListHecho from "../TareaListHecho";
import "../STYLES/bootstrap.css";
import "../STYLES/PagePrincipal.css";

export default class PaginaPrincipal extends Component {
  state = {
    nuevo: true,
    edit: {},
    jobs: tareas,
    input: ""
  };

  reloj = new Date();
  fecha =
    this.reloj.getDate() +
    "/" +
    this.reloj.getMonth() +
    "/" +
    this.reloj.getFullYear();
  hora = this.reloj.getHours() + ":" + this.reloj.getMinutes();

  agregarTarea = tarea => {
    let nuevasTareas = this.state.jobs;
    nuevasTareas.push(tarea);
    this.setState({ jobs: nuevasTareas }, () => {});
  };

  eliminarTarea = tarea => {
    let nuevasTareas = this.state.jobs.slice();
    nuevasTareas.forEach(item => {
      if (tarea.id === item.id) {
        let tareaFilter = nuevasTareas
          .slice()
          .filter(item1 => item1.id !== item.id);
        nuevasTareas = tareaFilter;
      }
    });

    this.setState({ jobs: nuevasTareas });
  };

  modificarTarea = tarea => {
    document.getElementById("titulo").value = tarea.title;
    this.setState({ nuevo: false, edit: tarea });
    this.eliminarTarea(tarea);
  };

  mayorID() {
    let may = 0;
    let nuevasTareas = this.state.jobs.slice();
    nuevasTareas.forEach(item => {
      if (item.id >= may) {
        may = item.id;
      }
    });
    return may;
  }

  aProceso = tarea => {
    let nuevasTareas = this.state.jobs.slice();
    nuevasTareas.forEach(item => {
      if (item.id === tarea.id) {
        item.tipo = 2;
      }
    });
    this.setState({ jobs: nuevasTareas });
  };

  aHecho = tarea => {
    let nuevasTareas = this.state.jobs.slice();
    nuevasTareas.forEach(item => {
      if (item.id === tarea.id) {
        item.tipo = 3;
      }
    });
    this.setState({ jobs: nuevasTareas });
  };

  aParaHacer = tarea => {
    let nuevasTareas = this.state.jobs.slice();
    nuevasTareas.forEach(item => {
      if (item.id === tarea.id) {
        item.tipo = 1;
      }
    });
    this.setState({ jobs: nuevasTareas });
  };

  handleSubmit = e => {
    e.preventDefault();
    let tarea = {
      id: this.mayorID() + 1,
      title: this.state.input,
      fecha: this.fecha,
      hora: this.hora,
      tipo: 1
    };
    this.agregarTarea(tarea);
  };

  handleEdit = e => {
    e.preventDefault();

    let tarea = {
      id: this.state.edit.id,
      title: this.state.input,
      fecha: this.fecha,
      hora: this.hora,
      tipo: this.state.edit.tipo
    };

    console.log(this.state.edit.id);

    this.agregarTarea(tarea);
    this.setState({ nuevo: true });
  };

  handleChange = e => {
    let newChange = this.state.input;
    newChange = e.target.value;
    this.setState({ input: newChange });
    console.log(this.state);
  };

  render() {
    if (this.state.nuevo) {
      return (
        <div>
          <h1> <b>MI TABLERO</b> </h1>

          <form onSubmit={this.handleSubmit}>
            <input
              type="text"
              id="titulo"
              className= "inputI"
              value={this.state.jobs.title}
              onChange={this.handleChange}
            />

            <button type="submit" className="b2 btn btn-success btn-lg">
              {" "}
              Guardar{" "}
            </button>
          </form>

          <div className="DivGeneral">
            <TareaListPorHacer
              tareas={this.state.jobs}
              onDelete={this.eliminarTarea}
              onEdit={this.modificarTarea}
              aProcess={this.aProceso}
            />

            <TareaListEnProceso
              tareas={this.state.jobs}
              onDelete={this.eliminarTarea}
              onEdit={this.modificarTarea}
              aHecho={this.aHecho}
              aParaHacer={this.aParaHacer}
            />

            <TareaListHecho
              tareas={this.state.jobs}
              onDelete={this.eliminarTarea}
              onEdit={this.modificarTarea}
              aProcess={this.aProceso}
            />
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <h1> MI TABLERO </h1>

          <form onSubmit={this.handleEdit}>
            <input
              type="text"
              id="titulo"
              name="title"
              value={this.state.jobs.title}
              onChange={this.handleChange}
            />

            <button type="submit" className="btn btn-success">
              {" "}
              Guardar{" "}
            </button>
          </form>

          <tabla>
            <tr>
              <td>
                <h1> Cosas para hacer </h1>
                <TareaListPorHacer
                  tareas={this.state.jobs}
                  onDelete={this.eliminarTarea}
                  onEdit={this.modificarTarea}
                  aProcess={this.aProceso}
                />
              </td>

              <td>
                <h1> En Proceso</h1>
                <TareaListEnProceso
                  tareas={this.state.jobs}
                  onDelete={this.eliminarTarea}
                  onEdit={this.modificarTarea}
                  aHecho={this.aHecho}
                  aParaHacer={this.aParaHacer}
                />
              </td>

              <td>
                <h1> Hecho </h1>
                <TareaListHecho
                  tareas={this.state.jobs}
                  onDelete={this.eliminarTarea}
                  onEdit={this.modificarTarea}
                  aProcess={this.aProceso}
                />
              </td>
            </tr>
          </tabla>
        </div>
      );
    }
  }

  /* Sé que puedo mejorarlo, en dos etapas del codigo podria hacer una reutilizacion */
}
