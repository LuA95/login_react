import React, { Component } from 'react';
import ShowUSER from '../ShowUSER';
import {Link} from 'react-router-dom';
import '../STYLES/bootstrap.css';

export default class Home extends Component {

   constructor(props) {
   super (props);
   this.state = { form : props.location.state
   }}
    render() {
        return (
            <div>
                <div className = "col-6">
                <ShowUSER state= {this.state.form} />
                </div>
                <div className = "col-6">
                <Link className= "btn btn-primary" to="/MisTareas">
                    Ingresar a mis tareas
                </Link>
                </div>
                
            </div>
        )
    }
}
