import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import LoginForm from "./LoginForm";
import Home from "./PAGES/Home";
import PaginaPrincipal from "./PAGES/PaginaPrincipal";

function app() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={LoginForm} />
          <Route exact path="/Home" component={Home} />
          <Route exact path="/MisTareas" component={PaginaPrincipal} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default app;
